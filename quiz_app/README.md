### Workflow of Project
Step 1: We'll fetch questions from the Open Trivia DB API.

Step 2: For each fetched question, we'll create a different object using a Question class. All these Question objects will be appended to a question_bank list.

Step 3: This question_bank will be passed to the brain of the application, QuizBrain and a quiz object will be created. This class will be responsible for checking if there are more questions, for getting the next question, calculating the score, and so on.

Step 4: Finally, this quiz object will be passed to the QuizInterface class, and the user will be able to interact with it.